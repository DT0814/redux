const initState = {
  userDetails: {},
  linkedIds:[]
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_USER_DETAILS':
      return {
        ...state,
        userDetails: action.userDetails
      };
    case "LINKED_USER":
      console.log(action)
      console.log(state.linkedIds)
      return {
        ...state,
        linkedIds: [...state.linkedIds,action.linkedId]
      };
    default:
      return state
  }
};
