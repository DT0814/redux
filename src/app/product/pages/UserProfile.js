import React from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { clickLike, getUserDetails } from "../actions/userActions";

class UserProfile extends React.Component {
  componentDidMount() {
    this.props.getUserDetails(this.props.match.params.id);
  }

  handlerClickLike() {
    this.props.clickLike(this.props.userDetails.name, this.props.match.params.id);
    this.forceUpdate();
  }

  render() {
    const user = this.props.userDetails;
    const liked = this.props.linkedIds.includes(this.props.match.params.id);
    return (
      <div>
        <p>
          UserName:{user.name}
        </p>
        <p>
          Gender:{user.gender}
        </p>
        <p>
          Description:{user.description}
        </p>
        <button onClick={this.handlerClickLike.bind(this)} disabled={liked}
                style={{ width: '100px', backgroundColor: 'grey', height: '50px' }}>
          {
            liked
              ? 'liked'
              : 'like'
          }
        </button>

      </div>
    )
  }
}

const mapStateToProps = state => ({
  userDetails: state.user.userDetails,
  linkedIds: state.user.linkedIds
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserDetails,
  clickLike,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
